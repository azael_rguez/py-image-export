import unittest
import inputs
from pyimageexport import PyImageExport


class RGBTestCase(unittest.TestCase):

    def test_png_base64(self):
        img = PyImageExport()
        image = img.export(
            input_image=inputs.PNG, input_format="png",
            output_format="base64")
        self.assertEqual(inputs.BASE64, image)

    def test_png_blob(self):
        img = PyImageExport()
        image = img.export(
            input_image=inputs.PNG, input_format="png",
            output_format="blob")
        self.assertEqual(inputs.BLOB, image)

    def test_png_jpeg(self):
        img = PyImageExport()
        image = img.export(
            input_image=inputs.PNG, input_format="png",
            output_format="jpeg", output_name="image",
            output_path="./tests")
        self.assertEqual(inputs.JPEG, image)

    def test_png_gif(self):
        img = PyImageExport()
        image = img.export(
            input_image=inputs.PNG, input_format="png",
            output_format="gif", output_name="image",
            output_path="./tests")
        self.assertEqual(inputs.GIF, image)

    def test_jpeg_gif(self):
        img = PyImageExport()
        image = img.export(
            input_image=inputs.JPEG, input_format="JPEG",
            output_format="gif", output_name="image",
            output_path="./tests")
        self.assertEqual(inputs.GIF, image)


if __name__ == '__main__':
    unittest.main()
